'use strict';

var moment = require('moment');
var _ = require('lodash');
var chalk = require('chalk');
var yeoman = require('yeoman-generator');

var ReactComponentGenerator = yeoman.generators.Base.extend({

	initializing: function() {
		this.pkg = require('../package.json');
	},

	prompting_init: function() {
		var done = this.async();

		this.log(
			'\n' + chalk.bold.underline('Welcome to the AlphaStack React Component generator') +
			'\n' + 'This generator ' + chalk.red('MUST') + ' be run from the root directory' +
			'\n'
		);

		var prompts = [{
			type: 'input',
			name: 'projectName',
			message: 'ComponentName',
			default: 'MyComponent'
		}];

		this.prompt(prompts, function (props) {
			_.extend(this, props);
			this.packageName = _.kebabCase(_.deburr(this.projectName));
			if (this.packageName.slice(0, 6) !== 'react-') {
				this.packageName = 'react-' + this.packageName;
			}
			this.componentName = _.capitalize(_.camelCase(this.projectName));
			this.currentYear = new Date().getFullYear();
			done();
		}.bind(this));
	},

	prompting_names: function() {
		var done = this.async();
		var currentDate = moment().format('l');

		var prompts = [{
			type: 'input',
			name: 'packageName',
			message: 'What is the Directory (i.e. Billing, Users, etc.) for your component?',
			default: (this.componentName.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase(); }).split(' ')[1] + 's')
		}, {
			type: 'input',
			name: 'developerName',
			message: 'What is your ' + chalk.blue('Github') + ' username? (for file creation comment)',
			default: 'alpha-stack'
		}, {
			type: 'confirm',
			name: 'relay',
			message: 'Should a Relay container be generated (will include viewer { id })',
			default: true
		}, {
			type: 'input',
			name: 'timeStamp',
			message: 'Date of file creation',
			default: currentDate
		}];

		this.prompt(prompts, function (props) {
			_.extend(this, props);
			done();
		}.bind(this));
	},

	prompting_project: function() {
		var done = this.async();

		var prompts = [{
			type: 'input',
			name: 'createDirectory',
			message: 'Is this file a Container, Component or Form?',
			default: 'container'
		}];

		var dirName;

		this.prompt(prompts, function (props) {
			_.extend(this, props);
			if (props.createDirectory === 'container') {
				this.dirName = 'src/containers/' + this.packageName;
				this.destinationRoot('src/containers/' + this.packageName);
			} else if (props.createDirectory === 'component') {
				this.dirName = 'src/components/' + this.packageName;
				this.destinationRoot('src/components/' + this.packageName);
			} else {
			  this.log('forms are not currently supported')
      }
			this.log('\n');
			done();
		}.bind(this));
	},

	writing: {
		component: function() {
			this.template('src/_Container.js', this.componentName + '.js');
		}
	},

	end: function() {
		this.log(
			'\n' + chalk.green.underline('Your ' + (this.createDirectory === 'container' ? 'container' : 'component') + ' is ready!') +
			'\n' +
			'\nYour React Component ' + chalk.green(this.componentName) + ' is in ' + chalk.blue(this.dirName + '/' + this.componentName + '.js') +
			'\n'
		);
	}

});

module.exports = ReactComponentGenerator;
