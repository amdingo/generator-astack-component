/**
 * Created by <%= developerName %> on <%= timeStamp %>.
 */

// TODO: DON'T FORGET TO ADD THIS FILE TO ANY RELEVANT index.js FILES!

import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Note from 'components/Shared/Note';
import { Page, PageHeader, PageContent } from 'components/Shared/Layouts';
import Nav from 'components/Shared/Nav';

export default class <%= componentName %> extends Component {
  static propTypes = {

  };

  static contextTypes = {
    router: PropTypes.object,
    store: PropTypes.object,
  };

<% if (relay) { %>  // TODO: CORRECT getRoute function

  static getRoute = ({ params }) => ({
    params,
    queries: {
      viewer: () => Relay.QL`query { viewer }`,
    },
    name: `<%= componentName.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase(); }) %> Route`,
  });

<% } %>  // TODO: CORRECT getNav function

  static getNav = () => (
    <Nav>
      <Nav.Group>
        {/*
        Nav Routes Go Here
        <Nav.Item to="somewhere" />
        */}
        <Nav.Item to="home" />
      </Nav.Group>
    </Nav>
  );

  render() {

    return (
      <Page>
        <PageHeader nav={<%= componentName %>.getNav()} />
        <PageContent>
        <Helmet title="<%= componentName.split(/(?=[A-Z])/).join(' ') %>" />
          <Grid style={{ float: `left` }}>
            <Row>
              <Note className="clearfix">
                <Col lg={12}>
                  <h4><b><%= componentName.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase(); }) %> Coming Soon!</b></h4>
                  <Col lg={12}>
                    <h5>This Component was generated with no Relay container</h5>
                  </Col>
                </Col>
              </Note>
            </Row>
          </Grid>
        </PageContent>
      </Page>
    );
  }
}


