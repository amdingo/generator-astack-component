/**
 * Created by <%= developerName %> on <%= timeStamp %>.
 */

// TODO: DON'T FORGET TO ADD THIS FILE TO ANY RELEVANT index.js FILES!

import React, { Component, PropTypes } from 'react';
<% if (!relay) { %>// <% } %>import Relay from 'react-relay-plus';
import Helmet from 'react-helmet';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Note from 'components/Shared/Note';
import { Page, PageHeader, PageContent } from 'components/Shared/Layouts';
import Nav from 'components/Shared/Nav';

<% if (!relay) { %>export default <% } %>class <%= componentName %> extends Component {
  static propTypes = {
<% if (relay) { %>    viewer: PropTypes.object,<% } %>
  };

  static contextTypes = {
    router: PropTypes.object,
    store: PropTypes.object,
  };

  <% if (relay) { %>  // TODO: CORRECT getRoute function

    static getRoute = ({ params }) => ({
    params,
    queries: {
    viewer: () => Relay.QL`query { viewer }`,
  },
    name: `<%= componentName.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase(); }) %> Route`,
  });

    <% } %>  // TODO: CORRECT getNav function

  static getNav = () => (
  <Nav>
    <Nav.Group>
      {/*
       Nav Routes Go Here
       <Nav.Item to="somewhere" />
       */}
      <Nav.Item to="home" />
    </Nav.Group>
  </Nav>
  );

  render() {
<% if (relay) { %>    const { viewer } = this.props;<% } %>
    return (
      <Page>
        <PageHeader nav={<%= componentName %>.getNav()} />
        <PageContent>
          <Helmet title="<%= componentName.split(/(?=[A-Z])/).join(' ') %>" />
          <Grid style={{ float: `left` }}>
            <Row>
              <Note className="clearfix">
                <Col lg={12}>
                <h4><b><%= componentName.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase(); }) %> Coming Soon!</b></h4>
                  <% if (relay) { %><Col lg={12}>
                    <h5>id from viewer: {viewer.id}</h5>
                    </Col><% } %><% if (!relay) { %><Col lg={12}>
                      <h5>This Component was generated with no Relay container</h5>
                  </Col><% } %>
                </Col>
              </Note>
            </Row>
          </Grid>
        </PageContent>
      </Page>
    );
  }
}

<% if (relay) { %>// TODO: CORRECT FRAGMENTS

  export default Relay.createContainer(<%= componentName %>, {
    fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `
  }
});<% } %>
